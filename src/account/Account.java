package account;

import static java.lang.String.format;

import client.Client;

public abstract class Account implements IAccount {

    private static final int DEFAULT_AGENCY = 001;
    private static int SEQUENTIAL = 1;

    protected int agency;
    protected int number;
    protected double balance;
    protected Client client;


    protected Account(Client client) {
        this.agency = Account.DEFAULT_AGENCY;
        this.number = SEQUENTIAL++;
        this.client = client;
    }

    public int getAgency() {
        return agency;
    }

    public int getNumber() {
        return number;
    }

    public double getBalance() {
        return balance;
    }

    public Client getClient() {
        return client;
    }

    @Override
    public void withdraw(double value) {
        balance -= value;
    }

    @Override
    public void deposit(double value) {
        balance += value;
    }

    @Override
    public void transfer(Account destinyAccount, double value) {
        this.withdraw(value);
        destinyAccount.deposit(value);
    }

    @Override
    public void extractPrintAccountChecking() {
        System.out.println("Print extract Account Checking");
        infoExtractPrint();
    }

    @Override
    public void extractPrintAccountSavings() {
        System.out.println("Print extract Account Savings");
        infoExtractPrint();
    }

    protected void infoExtractPrint() {
        System.out.println(format("Client: %s", this.client.getName()));
        System.out.println(format("Agency: %d", this.agency));
        System.out.println(format("Number: %d", this.number));
        System.out.println(format("Balance: %.2f", this.balance));
        System.out.println();
    }
}
