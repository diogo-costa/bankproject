package App.java;

import account.Account;
import account.AccountChecking;
import account.AccountSavings;
import client.Client;

public class App {
    public static void main(String[] args) throws Exception {

        Client client = new Client();
        client.setName("John Doe");

        Account ac = new AccountChecking(client);
        ac.deposit(1000);

        Account as = new AccountSavings(client);

        ac.transfer(as, 150);
        as.deposit(2000);

        ac.extractPrintAccountChecking();
        as.extractPrintAccountSavings();

    }
}
